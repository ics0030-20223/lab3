#!/usr/bin/env python3
import sys

import numpy as np
import pandas as pd
# STUDENT SHALL ADD NEEDED IMPORTS
#from sklearn.linear_model import LogisticRegression
#from sklearn.preprocessing import StandardScaler
#from sklearn.neighbors import KNeighborsClassifier

from common import describe_data, test_env


def read_data(file):
    """Return pandas dataFrame read from Excel file"""
    try:
        return pd.read_excel(file)
    except FileNotFoundError:
        sys.exit('ERROR: ' + file + ' not found')


def preprocess_data(df, verbose=False):
    y_column = 'In university after 4 semesters'

    # Features can be excluded by adding column name to list
    drop_columns = []

    categorical_columns = [
        'Faculty',
        'Paid tuition',
        'Study load',
        'Previous school level',
        'Previous school study language',
        'Recognition',
        'Study language',
        'Foreign student'
    ]

    # Handle dependent variable
    if verbose:
        print('Missing y values: ', df[y_column].isna().sum())

    y = df[y_column].values
    # Encode y. Naive solution
    y = np.where(y == 'No', 0, y)
    y = np.where(y == 'Yes', 1, y)
    y = y.astype(float)

    # Drop also dependent variable variable column to leave only features
    drop_columns.append(y_column)
    df = df.drop(labels=drop_columns, axis=1)

    # Remove drop columns for categorical columns just in case

    categorical_columns = [
        i for i in categorical_columns if i not in drop_columns]

    for column in categorical_columns:
        df = pd.get_dummies(df, prefix=[column], columns=[column])

    # Handle missing data. At this point only exam points should be missing
    # It seems to be easier to fill whole data frame as only particular columns
    if verbose:
        describe_data.print_nan_counts(df)


    for column in df:
        df[column] = df[column].fillna(0)

    if verbose:
        describe_data.print_nan_counts(df)

    # Return features data frame and dependent variable
    return df, y


# STUDENT SHALL CREATE FUNCTIONS FOR LOGISTIC REGRESSION CLASSIFIER, KNN
# CLASSIFIER, SVM CLASSIFIER, NAIVE BAYES CLASSIFIER, DECISION TREE
# CLASSIFIER AND RANDOM FOREST CLASSIFIER


# def logistic(X, y) :
#
#     print('# Logistic regression test data')
#     scaler = StandardScaler()
#
#     X = scaler.fit_transform(X)
#     y = scaler.transform(y)
#
#     model = LogisticRegression()
#     model.fit(X, y)


#def Knearest (X, y):




if __name__ == '__main__':
    modules = ['numpy', 'pandas', 'sklearn']
    test_env.versions(modules)

    students = read_data('data/students.xlsx')
    describe_data.print_overview(students, file = 'results/students_overview.txt')
    describe_data.print_categorical(students, file='results/students_categorical_data.txt')

    students_X, students_y = preprocess_data(students)

    #logistic(students_X, students_y)
    # STUDENT SHALL CALL CREATED CLASSIFIERS FUNCTIONS

    print('Done')
